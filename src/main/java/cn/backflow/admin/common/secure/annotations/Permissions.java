package cn.backflow.admin.common.secure.annotations;

import java.lang.annotation.*;

/**
 * 权限验证注解
 * Created by hunan on 2017/5/21.
 */
@Documented
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Permissions {
    String value();
}
